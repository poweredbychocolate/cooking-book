package persistenceBean;

import java.util.List;

import javax.ejb.Remote;
import recipes.CookingRecipe;
import recipes.Ingredient;
/**
 * Remote interface processing recipes, ingredients and related operation
 * @author Dawid
 * @version 1
 */
@Remote
public interface RecipleBeanRemote {
	/**
	 * Store single {@link CookingRecipe} in database
	 * @param name 
	 * @param category
	 * @param image image as binary object
	 * @param prepare
	 * @param ingredients
	 * @return true if save successful, otherwise false 
	 */
	public boolean saveReciple(String name,String category, byte[] image,String prepare,List<Ingredient> ingredients);
	/**
	 * Search {@link CookingRecipe} that name have substring passed as parameter 
	 * @param name string that is name or name fragment
	 * @return matched {@link CookingRecipe} list
	 */
	public List<CookingRecipe> getRecipeWithName(String name);
	/**
	 * Search recipe that have {@link Ingredient} from passed list. 
	 * @param list {@link Ingredient} list that will by search
	 * @return matched {@link CookingRecipe} list
	 */
	public List<CookingRecipe> getRecipeWithIngredients(List<String> list);
	/**
	 * Fully read single {@link CookingRecipe} 
	 * @param id the entity id 
	 * @return {@link CookingRecipe} witch selected id if exist
	 */
	public CookingRecipe findRecipleById(long id);
	/**
	 * Get max available recipe index
	 * @return the highest index
	 */
	public Long getRecipeMaxIndex();
	/**
	 * Check if entity with given id exist
	 * @param id {@link CookingRecipe} id 
	 * @return true if exist, otherwise false 
	 */
	public boolean isRecipeId(long id);
	/**
	 * 
	 * @param id
	 * @return
	 */
	byte[] findImageById(long id);

	
}
