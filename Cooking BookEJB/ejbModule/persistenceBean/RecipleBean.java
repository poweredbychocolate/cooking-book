package persistenceBean;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import recipes.CookingRecipe;
import recipes.CookingRecipe_;
import recipes.Ingredient;

/**
 * Session Bean implementation class recipleBean
 */
@Stateless
public class RecipleBean implements RecipleBeanRemote {
	private CookingRecipe lastRecipe = null;

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager menager;

	public RecipleBean() {
	}

	@Override
	public boolean saveReciple(String name,String category, byte[] image, String prepare,List<Ingredient> ingredients) {
		try {
			CookingRecipe recipe = new CookingRecipe();
			System.err.println(recipe.getId());
			recipe.setName(name);
			recipe.setCategory(category);
			//ByteArrayOutputStream byteImage = new ByteArrayOutputStream();
			//ImageIO.write(ImageIO.read(image),image.getName().substring(image.getName().lastIndexOf(".")+1), byteImage);
			//recipe.setImage(byteImage.toByteArray());
			recipe.setImage(image);
			recipe.setPrepare(prepare);
			for (Ingredient ingredient : ingredients) {
				ingredient.setRecipe(recipe);
			}
			recipe.setIngredients(ingredients);
			menager.persist(recipe);
			System.err.println("[Save reciple]");
			return true;
		} catch (Exception e) {
			System.err.println("[Reciple not saved]");
			return false;
		}
	}

	@Override
	public List<CookingRecipe> getRecipeWithName(String name) {
		CriteriaBuilder bulider = menager.getCriteriaBuilder();
		// Create query object
		CriteriaQuery<CookingRecipe> query = bulider.createQuery(CookingRecipe.class);
		// Set form clause of query
		Root<CookingRecipe> recipe = query.from(CookingRecipe.class);
		// passing in the query root, to set the SELECT clause of the query
		query.select(recipe);
		// using like with name
		query.where(bulider.like(recipe.get(CookingRecipe_.name), new String("%" + name + "%")));
		query.orderBy(bulider.asc(recipe.get(CookingRecipe_.name)));
		TypedQuery<CookingRecipe> result = menager.createQuery(query);
		return result.getResultList();
	}

	@Override
	public List<CookingRecipe> getRecipeWithIngredients(List<String> list) {

		String q = "SELECT r.Id, r.name, r.category FROM CookingRecipe r INNER JOIN r.ingredients i "
				+ "WHERE i.name IN :iList GROUP BY r.name ORDER BY  COUNT(r.name) DESC, r.name ASC ";
		TypedQuery<Object[]> query = menager.createQuery(q, Object[].class);
		query.setParameter("iList", list);
		List<CookingRecipe> returnList = new LinkedList<CookingRecipe>();
		List<Object[]> results = query.getResultList();
		for (Object[] object : results) {
			System.err.println("Object : [" + object[0] + ", " + object[1] + ", " + object[2] + "]");
			CookingRecipe recipe = new CookingRecipe();
			recipe.setId((long) object[0]);
			recipe.setName((String) object[1]);
			recipe.setCategory((String) object[2]);
			returnList.add(recipe);
		}
		return returnList;
	}

	@Override
	public CookingRecipe findRecipleById(long id) {
		if (lastRecipe == null || lastRecipe.getId() != id) {
			// if mark ingredients list fetch type as eager
			// return menager.find(CookingRecipe.class,id);
			// if mark ingredients list fetch type as lazy XD
			CriteriaBuilder builder = menager.getCriteriaBuilder();
			CriteriaQuery<CookingRecipe> query = builder.createQuery(CookingRecipe.class);
			Root<CookingRecipe> recipe = query.from(CookingRecipe.class);
			recipe.fetch(CookingRecipe_.ingredients, JoinType.INNER);
			query.select(recipe);
			query.where(builder.equal(recipe.get(CookingRecipe_.Id), id));
			lastRecipe = menager.createQuery(query).getSingleResult();
		}
		return lastRecipe;

	}

	@Override
	public Long getRecipeMaxIndex() {
		CriteriaBuilder builder = menager.getCriteriaBuilder();
		// set return type as long
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		// set select class
		Root<CookingRecipe> recipe = query.from(CookingRecipe.class);
		// select max index
		query.multiselect(builder.max(recipe.get(CookingRecipe_.Id)));
		return menager.createQuery(query).getSingleResult();
	}

	@Override
	public boolean isRecipeId(long id) {
		CriteriaBuilder builder = menager.getCriteriaBuilder();
		// set retutn type as long
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		// set select flass
		Root<CookingRecipe> recipe = query.from(CookingRecipe.class);
		// select max index
		query.select(recipe.get(CookingRecipe_.Id));
		query.where(builder.equal(recipe.get(CookingRecipe_.Id), id));
		try {
			menager.createQuery(query).getSingleResult();
			return true;
		} catch (Exception e) {
			return false;
		}

	}
	@Override
	public byte[] findImageById(long id) {
		if (lastRecipe == null || lastRecipe.getId() != id) {
			CriteriaBuilder builder = menager.getCriteriaBuilder();
			CriteriaQuery<byte[]> query = builder.createQuery(byte[].class);
			Root<CookingRecipe> recipe = query.from(CookingRecipe.class);
			query.select(recipe.get(CookingRecipe_.image));
			query.where(builder.equal(recipe.get(CookingRecipe_.Id), id));
			return menager.createQuery(query).getSingleResult();
		}
		else
		{
			return lastRecipe.getImage();
		}

	}
	

}
