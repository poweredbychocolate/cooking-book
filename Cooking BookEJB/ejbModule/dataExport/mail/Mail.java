package dataExport.mail;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;

import dataExport.DataExport;
import recipes.Ingredient;

/**
 * Session Bean implementation class Mail
 * @author Dawid
 * @since 27.01.2018
 * @version 1
 */
@Stateful
public class Mail implements MailRemote {

	private String path;
	private String servletName;
	private String parmName;
	private String linksTitle;
	private String sendTo;
	private String subject;
	private Map<String, Long> namesAndId;
	private boolean hyperliks=false;
	private int ready=0;
	////
	private String ingredientsTile;
	private List<Ingredient> ingredientsList;
	private boolean list=false;
	///
	private String content;
    /**
     * Default constructor. 
     */
    public Mail() {
    }
    @Override
    public void configureAddress(String sendTo, String subject)
    {
    	this.sendTo=sendTo;
    	this.subject=subject;
    	this.ready++;
    }
	@Override
	public void configureHyperlink(String path, String servletName, String requestParm,String title,Map<String, Long> nameAndId) {
		this.path=path;
		this.servletName=servletName;
		this.parmName=requestParm;
		this.linksTitle=title;
		this.namesAndId=nameAndId;
		this.hyperliks=true;
	}	

	@Override
	public void configureList(String title,List<Ingredient> list) {
		this.ingredientsTile=title;
		this.ingredientsList=list;
		this.list=true;
		this.ready++;
	}

	/**
     * @see DataExport#bulid()
     */
	@Override
	public void bulid() {
		StringBuilder mailString = new StringBuilder();
		mailString.append("<html>"+System.lineSeparator());
		mailString.append("<body>"+System.lineSeparator());
		mailString.append("<div>"+System.lineSeparator());
		if(list) {
			mailString.append("<h1>"+ingredientsTile+"</h1>"+System.lineSeparator());
			mailString.append("<table>"+System.lineSeparator());
			for (Ingredient ingredient : ingredientsList) {
			mailString.append("<tr><th align=\"left\">&#x25CF;</th><th align=\"left\">"+ingredient.getName()+"</th><th align=\"left\">"+ingredient.getValue()+"</th><th align=\"left\">"+ingredient.getUnit()+"</th></tr>"+System.lineSeparator());
			}
			mailString.append("</table>"+System.lineSeparator());
		}
		if(hyperliks) {
			mailString.append("<h1>"+linksTitle+"</h1>"+System.lineSeparator());
			mailString.append("<ul style=\"list-style-type:square\">"+System.lineSeparator());
			for (String name : namesAndId.keySet()) {
				mailString.append("<li><a href=\""+path+"/"+servletName+"?"+parmName+"="+namesAndId.get(name)+"\">"+name+"</a></li>"+System.lineSeparator());
			}
			mailString.append("</ul>"+System.lineSeparator());
		}
		mailString.append("</div>"+System.lineSeparator());
		mailString.append("</body>"+System.lineSeparator());
		mailString.append("</html>"+System.lineSeparator());
		content=mailString.toString();
	}	

	@Override
	public String get() {
		return content;
	}

	@Override
	public boolean isReady() {
		return this.ready==2;
	}
	@Override
	public String getSendTo() {
		return sendTo;
	}
	@Override
	public String getSubject() {
		return subject;
	}

}
