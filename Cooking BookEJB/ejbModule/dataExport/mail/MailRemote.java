package dataExport.mail;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import dataExport.DataExport;
import recipes.Ingredient;

@Remote
/**
 * Mail Data Object
 * @author Dawid
 *
 */
public interface MailRemote extends DataExport{
	/**
	 * Set mail recipient address and subject
	 * @param sendTo
	 * @param subject
	 */
	public void configureAddress(String sendTo, String subject);
	/**
	 * Append ingredients list to mail, this method together with {@link #configureAddress(String, String)} mark mail as ready 
	 * @param title
	 * @param list
	 */
	public void configureList(String title, List<Ingredient> list);
	/**
	 * Append hyperlinks list
	 * @param path
	 * @param servletName
	 * @param requestParm
	 * @param title
	 * @param nameAndId
	 */
	public void configureHyperlink(String path, String servletName, String requestParm, String title,
			Map<String, Long> nameAndId);
	/**
	 * Return recipient address
	 * @return
	 */
	public String getSendTo();
	/**
	 * Return mail subject
	 * @return
	 */
	public String getSubject();



}
