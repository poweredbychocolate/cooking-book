/**
 *  Basic data export unit, allow check if minimal requirements, bulid content and get bulid object

 */
package dataExport;

import javax.ejb.Remote;

/**
 * Basic interface using by {@link SendDataBeanRemote} to configure, create and delivery data 
 * @author Dawid
 * @since 25.01.2018
 * @version 1
 */
@Remote

public interface DataExport {
	/**
	 * Generate embedded data object  
	 */
	public void bulid();
	/**
	 * Return generated data object  
	 * @return data object
	 */
	public Object get();
	/**
	 * Check if all required data that will be used to generate object is set
	 * @return true if object can be bulid otherwise return false
	 */
	public boolean isReady();
}
