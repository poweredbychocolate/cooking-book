/**
 * Asynchronous class allow send mail in background 
 */
package dataExport;

import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import dataExport.mail.MailRemote;

/**
 * Session Bean implementation class DocumentBean
 * 
 * @author Dawid
 * @since 28.01.2018
 * @version 1
 */
@Stateless
@Asynchronous
public class SendDataBean implements SendDataBeanRemote {
	// Get resource that is configure in server as mail
	@Resource(name = "java:/Mail")
	private Session session;

	/**
	 * Default constructor.
	 */
	public SendDataBean() {
	}

	@Override
	public void send(DataExport data) {
		if (data.isReady()) {
			/// ] if data is instance MailRemote send html as mail
			if (data instanceof MailRemote) {
				try {
					data.bulid();
					MimeMessage msg = new MimeMessage(session);
					msg.setHeader("Content-Type", "text/plain; charset=UTF-8");
					msg.setRecipients(RecipientType.TO, ((MailRemote) data).getSendTo());
					msg.setSubject(((MailRemote) data).getSubject(), "utf-8");
					msg.setSentDate(new Date());
					msg.setContent(data.get(), "text/html; charset=UTF-8");
					Transport.send(msg);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			/// ] if data is instance MailRemote send html as mail
			//// allow add other send method
		}
	}

}
