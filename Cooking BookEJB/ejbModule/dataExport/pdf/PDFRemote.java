package dataExport.pdf;

import java.util.List;

import javax.ejb.Remote;
import dataExport.DataExport;
import recipes.Ingredient;

@Remote
/**
 * Generate PDF file 
 * @author Dawid
 *
 */
public interface PDFRemote extends DataExport {
	/**
	 * Add recipe name and prepare, this method set ready flag to true
	 * @param title
	 * @param prepareTitle
	 * @param prepare
	 */
	public void addBase(String title, String prepareTitle, String prepare);
	/**
	 * add ingredient list that will be displayed as table 
	 * @param listTitle
	 * @param list
	 */
	public void appendIngrediends(String listTitle, List<Ingredient> list);
	/**
	 * Include hyperlink witch allow page consist recipe 
	 * @param hyperlink
	 */
	public void includeHyperlink(String hyperlink);
}
