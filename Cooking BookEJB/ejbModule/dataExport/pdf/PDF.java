package dataExport.pdf;

import java.io.IOException;
import java.util.List;

import javax.ejb.Stateful;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Link;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.UnitValue;

import recipes.Ingredient;

/**
 * Session Bean implementation class PDF
 */
@Stateful
public class PDF implements PDFRemote {
	private String title, prepareName, prepare, link, listTitle;
	private boolean ready=false;
	private List<Ingredient> list;
	private byte[] fileStream;

	/**
	 * Default constructor.
	 */
	public PDF() {
	}

	@Override
	public void addBase(String title, String prepareTitle, String prepare) {
		this.title = title;
		this.prepare = prepare;
		this.prepareName = prepareTitle;
		this.ready=true;
	}

	@Override
	public void appendIngrediends(String listTitle, List<Ingredient> list) {
		this.list = list;
		this.listTitle = listTitle;
	}

	@Override
	public void includeHyperlink(String hyperlink) {
		this.link = hyperlink;
	}

	@Override
	public void bulid() {
		// throw BaseNotFoud if addBase method not call before
		if (this.title == null || this.prepare == null)
			return;
		try {
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			Document document = new Document(new PdfDocument(new PdfWriter(output)), PageSize.A4);

			PdfFont font = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN, PdfEncodings.CP1250, true, true);

			//// add recipe title
			document.add(new Paragraph(title).setBold().setFontSize(25.0f).setBackgroundColor(Color.LIGHT_GRAY)
					.setFont(font));
			//// add ingrdients
			if (this.list != null && !this.list.isEmpty()) {
				document.add(new Paragraph(listTitle).setBold().setFont(font));
				Table table = new Table(new UnitValue[] { new UnitValue(UnitValue.PERCENT, 70),
						new UnitValue(UnitValue.PERCENT, 15), new UnitValue(UnitValue.PERCENT, 15) });
				for (Ingredient ingredient : list) {
					table.addCell(ingredient.getName()).setHorizontalAlignment(HorizontalAlignment.LEFT);
					table.addCell(Double.toString(ingredient.getValue()))
							.setHorizontalAlignment(HorizontalAlignment.LEFT);
					table.addCell(ingredient.getUnit()).setHorizontalAlignment(HorizontalAlignment.LEFT);
				}
				document.add(table).setFont(font);
			}
			//// add name for prepare
			document.add(new Paragraph(prepareName).setBold().setFont(font));
			//// add prepare
			document.add(new Paragraph(prepare).setFont(font));
			//// add link
			if (this.link != null && !this.link.isEmpty()) {
				document.add(new Paragraph(new Link(link, PdfAction.createURI(link))));
			}
			document.close();
			fileStream=output.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public byte[] get() {
		
		return fileStream;
	}

	@Override
	public boolean isReady() {
		return ready;
	}

}
