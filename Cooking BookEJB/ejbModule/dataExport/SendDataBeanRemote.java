package dataExport;

import javax.ejb.Remote;

@Remote
/**
 * Class correspond to export data
 * 
 * @author Dawid
 * @since 28.01.2018
 * @version 1
 */
public interface SendDataBeanRemote {
	/**
	 * Send data object to user
	 * 
	 * @param data
	 *            object that will be send
	 */
	public void send(DataExport data);
}
