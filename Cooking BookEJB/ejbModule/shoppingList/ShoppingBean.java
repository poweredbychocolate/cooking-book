package shoppingList;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import recipes.Ingredient;

/**
 * Session Bean implementation class ShoppingBean
 */
@Stateful
public class ShoppingBean implements ShoppingBeanRemote {
	private Map<String, Long> recipeIdList;
	private Map<String, Ingredient> ingredientsList;

	/**
	 * Default constructor.
	 */
	public ShoppingBean() {
	}

	/**
	 * Create new HashMaps
	 */
	@PostConstruct
	private void init() {
		recipeIdList = new HashMap<String, Long>();
		ingredientsList = new HashMap<String, Ingredient>();
	}

	/**
	 * Clear all list and remove reference
	 */
	@PreDestroy
	private void clear() {
		recipeIdList.clear();
		ingredientsList.clear();
		recipeIdList = null;
		ingredientsList = null;
	}

	@Override
	public void addToList(long recipeId, String recipeName, List<Ingredient> recipeIngerdients) {
		recipeIdList.put(recipeName, recipeId);
		for (Ingredient ingredient : recipeIngerdients) {
			if (ingredientsList.containsKey(ingredient.getName())) {
				ingredientsList.get(ingredient.getName()).addToValue(ingredient.getValue());
			} else {
				try {
					ingredientsList.put(ingredient.getName(), (Ingredient) ingredient.clone());
				} catch (CloneNotSupportedException e) {
					
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void clearList() {
		recipeIdList.clear();
		ingredientsList.clear();

	}

	@Override
	public String  printToString() {
		StringBuilder s = new StringBuilder();
		s.append("[Names]"+System.lineSeparator());
		Set<String> set = recipeIdList.keySet();
		for (String string : set ) {
			s.append(string+System.lineSeparator());
		}
		s.append("[Id]"+System.lineSeparator());
		Collection<Long> col = recipeIdList.values();
		for (Long long1 : col) {
			s.append(long1+System.lineSeparator());
		}
		s.append("[Ingedients]"+System.lineSeparator());
		Collection<Ingredient> coIng =ingredientsList.values();
		for (Ingredient ingredient : coIng) {
			s.append("{"+ingredient.getName()+", "+ingredient.getValue()+", "+ingredient.getUnit()+"}"+System.lineSeparator());
		}
		return s.toString();
	}

	@Override
	public List<Ingredient> getIngredientsList() {
		return new LinkedList<Ingredient>(ingredientsList.values());
	}

	@Override
	public Map<String, Long> getIdAndName() {
		return recipeIdList;
	}


}
