package shoppingList;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import recipes.CookingRecipe;
import recipes.Ingredient;
/**
 * 
 * @author Dawid
 *	@version 1
 *@since 12.01.2018
 */
@Remote
public interface ShoppingBeanRemote {
	/**
	 * Add new {@link Ingredient} to list, {@link CookingRecipe} id and name is used to generate hyperlink
	 * @param recipeId {@link CookingRecipe} id 
	 * @param recipeName {@link CookingRecipe} name
	 * @param recipeIngerdients {@link Ingredient} list 
	 */
	public void addToList(long recipeId,String recipeName,List<Ingredient> recipeIngerdients);
	/**
	 * remove all element previously added to list
	 */
	public void clearList();
	/**
	 * Return shopping list
	 * @return {@link Ingredient} shopping list 
	 */
	public List<Ingredient> getIngredientsList();
	/**'
	 * Generate link to {@link CookingRecipe} selected by user 
	 * @param servletPath hyperlink that allow to access to servlet with open details page
	 * @return Hyperlink list that allow
	 */
	/**
	 * Return map that key is {@link CookingRecipe} name and value is a his id
 	 * @return {@link Map}
	 */
	public Map<String,Long> getIdAndName();

	/**
	 * Generate string that consist all ingredients, names and id
	 * @return 
	 */
	@Deprecated
	public String printToString();
}
