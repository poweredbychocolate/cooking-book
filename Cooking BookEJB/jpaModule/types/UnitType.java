package types;

import java.util.LinkedList;
import java.util.List;
/**
 * Application unit type 
 * @author Dawid
 *	@version 1
 */
public class UnitType {
	public static final String KG = "KG";
	public static final String LITER = "Liter";
	public static final String SPOON = "Spoon";
	private static List<String> unitTypes;
	static {
		unitTypes = new LinkedList<String>();
		unitTypes.add(KG);
		unitTypes.add(LITER);
		unitTypes.add(SPOON);
	}

	public static List<String> getUnitTypes() {
		return unitTypes;
	}
}
