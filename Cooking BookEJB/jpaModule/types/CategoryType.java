package types;

import java.util.LinkedList;
import java.util.List;
/**
 * Application category type
 * @author Dawid
 *	@version 1
 */
public class CategoryType {
	// public static final String NAME = "";
	public static final String BREAKFAST = "Breakfast";
	public static final String DESSERTS = "Desserts";
	public static final String DRINKS = "Drinks";
	////
	private static List<String> categoryTypes;
	static {
		categoryTypes = new LinkedList<String>();
		categoryTypes.add(BREAKFAST);
		categoryTypes.add(DESSERTS);
		categoryTypes.add(DRINKS);
	}

	public static List<String> getCategoryTypes() {
		return categoryTypes;

	}
}
