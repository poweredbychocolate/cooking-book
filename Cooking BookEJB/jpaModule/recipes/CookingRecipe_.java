package recipes;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * {@link CookingRecipe} Metamodel
 * 
 * @author Dawid
 * @version 1
 */
@StaticMetamodel(CookingRecipe.class)
public class CookingRecipe_ {

	public static volatile SingularAttribute<CookingRecipe, Long> Id;
	public static volatile SingularAttribute<CookingRecipe, String> category;
	public static volatile SingularAttribute<CookingRecipe, String> name;
	public static volatile SingularAttribute<CookingRecipe, String> prepare;
	public static volatile ListAttribute<CookingRecipe, Ingredient> ingredients;
	public static volatile SingularAttribute<CookingRecipe, byte[]> image;
}
