package recipes;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Ingredient
 * 
 * @author Dawid
 * @version 1
 */
@Entity
public class Ingredient implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ingredientId;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private double value;
	@Column(nullable = false)
	private String unit;

	@ManyToOne
	@JoinColumn(name = "recipe_id", nullable = false)
	private CookingRecipe recipe;

	/**
	 * Default constructor
	 */
	public Ingredient() {
		super();
	}

	/**
	 * 
	 * @return the ingredientId
	 */
	public long getIngredientId() {
		return this.ingredientId;
	}

	/**
	 * 
	 * @param ingredientId
	 *            the ingredientId to set
	 */
	public void setIngredientId(long ingredientId) {
		this.ingredientId = ingredientId;
	}

	/**
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return the value
	 */
	public double getValue() {
		return this.value;
	}

	/**
	 * 
	 * @param value
	 *            the value to set
	 */
	public void setValue(double value) {
		this.value = value;
	}

	/**
	 * 
	 * @return the unit
	 */
	public String getUnit() {
		return this.unit;
	}

	/**
	 * 
	 * @param unit
	 *            the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @return the recipe
	 */
	public CookingRecipe getRecipe() {
		return recipe;
	}

	/**
	 * @param recipe
	 *            the recipe to set
	 */
	public void setRecipe(CookingRecipe recipe) {
		this.recipe = recipe;
	}

	/**
	 * Clone {@link Ingredient} without cloning his id
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Ingredient clone = (Ingredient) super.clone();
		clone.setIngredientId(0);
		return clone;
	}
	/**
	 * Increases the value by the value passed
	 * @param increaseValue
	 */
	public void addToValue(double increaseValue){
		this.setValue(this.getValue()+increaseValue);
	}

}
