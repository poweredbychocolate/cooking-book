/**
 * 
 */
package recipes;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * {@link Ingredient} Metamodel
 * @author Dawid
 *	@version 1
 */
@StaticMetamodel(Ingredient.class)
public class Ingredient_ {
	public static volatile SingularAttribute<Ingredient, Long> ingredientId;
	public static volatile SingularAttribute<Ingredient, String> name;
	public static volatile SingularAttribute<Ingredient, Double> value;
	public static volatile SingularAttribute<Ingredient, String> unit;
	public static volatile SingularAttribute<Ingredient, CookingRecipe> recipe;
}
