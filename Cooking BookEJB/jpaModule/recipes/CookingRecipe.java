package recipes;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: CookingRecipe
 *@author Dawid
 *@version 2
 */
@Entity
public class CookingRecipe implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id;
	@Column(nullable = false)
	private String category;
	@Column(nullable = false)
	private String name;
	@Lob
	@Column(nullable = false)
	@Basic(fetch = FetchType.LAZY)
	private String prepare;
	
	@OneToMany(targetEntity = Ingredient.class, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "recipe", fetch = FetchType.LAZY)
	private List<Ingredient> ingredients;
	
	////> since version 2
	@Lob
	 @Column(nullable = false)
	 private byte[] image;

/**
 * Default constructor
 */
	public CookingRecipe() {
		super();
	}
	/**
	 * Return id
	 * @return {@link Long}
	 */
	public long getId() {
		return this.Id;
	}
/**
 * Set id
 * @param Id
 */
	public void setId(long Id) {
		this.Id = Id;
	}
/**
 * 
 * @return {@link String}
 */
	public String getCategory() {
		return this.category;
	}
/**
 * 
 * @param category
 */
	public void setCategory(String category) {
		this.category = category;
	}
/**
 * 
 * @return {@link String}
 */
	public String getName() {
		return this.name;
	}
/**
 * 
 * @param name
 */
	public void setName(String name) {
		this.name = name;
	}
/**
 * 
 * @return 
 */
	public String getPrepare() {
		return this.prepare;
	}
/**
 * 
 * @param prepare
 */
	public void setPrepare(String prepare) {
		this.prepare = prepare;
	}
/**
 * 
 * @return
 */
	public List<Ingredient> getIngredients() {
		return this.ingredients;
	}
/**
 * 
 * @param ingredients
 */
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	////>since verision 2
/**
 * Set binary image
 * @return the image
 */
public byte[] getImage() {
	return image;
}
/**	get binary image
 * @param image the image to set
 */
public void setImage(byte[] image) {
	this.image = image;
}
}
