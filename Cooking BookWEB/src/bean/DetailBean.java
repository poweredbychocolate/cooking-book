/**
 * This bean must be session scoped, because need keep recipe to allow add his recipe to shopping list
 */
package bean;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ResourceBundle;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dataExport.SendDataBeanRemote;
import dataExport.pdf.PDFRemote;
import persistenceBean.RecipleBeanRemote;
import recipes.CookingRecipe;

/**
 * Get single {@link CookingRecipe} and display using recipe_detail.xhtml
 * 
 * @author Dawid
 * @since 10.01.2018
 * @version 2
 */
public class DetailBean implements Serializable {

	private static final long serialVersionUID = 2L;
	private long recipeId = -1;
	private CookingRecipe lastRecipe;
	@EJB
	private RecipleBeanRemote recipeBean;
	@EJB
	private PDFRemote pdf;
	@EJB
	private SendDataBeanRemote document;

	/**
	 * id {@link CookingRecipe} that will be return
	 * 
	 * @param recipeId
	 *            the recipeId to set
	 */
	public void setRecipeId(long recipeId) {
		this.recipeId = recipeId;
	}

	/**
	 * Return {@link CookingRecipe} if entity with choosen id exist
	 * 
	 * @return the recipe
	 */
	public CookingRecipe getRecipe() {
		///] check if page is called by servlet get value from request parameter, if is not null set his as selected recipe id 
			String value = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("id");
			if (value!=null) {
				lastRecipe = recipeBean.findRecipleById(Long.parseLong(value));
			}
			///] check if page is called by servlet get value from request parameter, if is not null set his as selected recipe id 
			else
		lastRecipe = recipeBean.findRecipleById(recipeId);		
		return lastRecipe;
	}

	/**
	 * Generate pdf file from selected recipe and display it in browser
	 */
	public void makePDF() {
		//// get language properties, set mail subject according to user language
		//// preferences
		ResourceBundle bundle = FacesContext.getCurrentInstance().getApplication()
				.getResourceBundle(FacesContext.getCurrentInstance(), "formLang");
		//// ] get path that allow open recipe using servlet
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		String url = req.getRequestURL().toString();
		String path = url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath()
				+ "/Details?display=" + recipeId;
		//// ] get path that allow open recipe using servlet
		pdf.addBase(lastRecipe.getName(), bundle.getString("prepare"), lastRecipe.getPrepare());
		pdf.appendIngrediends(bundle.getString("ingredients"), lastRecipe.getIngredients());
		pdf.includeHyperlink(path);
		try {
			pdf.bulid();
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			byte[] bytes = (byte[]) pdf.get();
			response.reset();
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Length", String.valueOf(bytes.length));
			response.setHeader("Content-Disposition", "inline; filename=\"" + lastRecipe.getName() + ".pdf" + "\"");
			BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream());
			//// write pdf bytes to response outut stream 
			output.write(bytes);
			output.flush();
			output.close();
			FacesContext.getCurrentInstance().responseComplete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
