/**
 * Because class allow open page witch recipe details, has session scope.
 * Recipe id must be available when details page is called.
 */

package bean;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import persistenceBean.RecipleBeanRemote;
import recipes.CookingRecipe;
/**
 * Display recipe list, allow search by name and find recipes witch ingredients we already have
 * @author Dawid
 * @version 1
 * @since 09.01.2018
 */
public class ListBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String searchField;
	private String ingredient1;
	private String ingredient2;
	private String ingredient3;
	private String ingredient4;
	private String ingredient5;
	private CookingRecipe selectedRecipe;
	private List<CookingRecipe> cookingRecipes;
	@EJB
	private RecipleBeanRemote recipleBean;

	/**
	 * @return the cookingRecipes
	 */
	public List<CookingRecipe> getCookingRecipes() {
		return cookingRecipes;
	}

	/**
	 * @return the searchField
	 */
	public String getSearchField() {
		return searchField;
	}

	/**
	 * Get ingredients list ,check and return recipe list with have ingredients from list  
	 */
	public void searchIngredients() {
		List<String> list = new LinkedList<String>();
		if(!ingredient1.isEmpty()) list.add(ingredient1);
		if(!ingredient2.isEmpty()) list.add(ingredient2);
		if(!ingredient3.isEmpty()) list.add(ingredient3);
		if(!ingredient4.isEmpty()) list.add(ingredient4);
		if(!ingredient5.isEmpty()) list.add(ingredient5);
		if(!list.isEmpty())	
		this.cookingRecipes = recipleBean.getRecipeWithIngredients(list);
		
	}
	/**
	 * Search recipe witch name consist characters entered in {@link #searchField}
	 */
	public void searchTitle() {
		this.cookingRecipes = recipleBean.getRecipeWithName(searchField);
	}

	/**
	 * @param searchField
	 *            the searchField to set
	 */
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	/**
	 * @return the ingredient1
	 */
	public String getIngredient1() {
		return ingredient1;
	}

	/**
	 * @param ingredient1 the ingredient1 to set
	 */
	public void setIngredient1(String ingredient1) {
		this.ingredient1 = ingredient1;
	}

	/**
	 * @return the ingredient2
	 */
	public String getIngredient2() {
		return ingredient2;
	}

	/**
	 * @param ingredient2 the ingredient2 to set
	 */
	public void setIngredient2(String ingredient2) {
		this.ingredient2 = ingredient2;
	}

	/**
	 * @return the ingredient3
	 */
	public String getIngredient3() {
		return ingredient3;
	}

	/**
	 * @param ingredient3 the ingredient3 to set
	 */
	public void setIngredient3(String ingredient3) {
		this.ingredient3 = ingredient3;
	}

	/**
	 * @return the ingredient4
	 */
	public String getIngredient4() {
		return ingredient4;
	}

	/**
	 * @param ingredient4 the ingredient4 to set
	 */
	public void setIngredient4(String ingredient4) {
		this.ingredient4 = ingredient4;
	}

	/**
	 * @return the ingredient5
	 */
	public String getIngredient5() {
		return ingredient5;
	}

	/**
	 * @param ingredient5 the ingredient5 to set
	 */
	public void setIngredient5(String ingredient5) {
		this.ingredient5 = ingredient5;
	}

	/**
	 * @return the selectedRecipe
	 */
	public CookingRecipe getSelectedRecipe() {
		return selectedRecipe;
	}

	/**
	 * @param selectedRecipe the selectedRecipe to set
	 */
	public void setSelectedRecipe(CookingRecipe selectedRecipe) {
		this.selectedRecipe = selectedRecipe;
	}
}
