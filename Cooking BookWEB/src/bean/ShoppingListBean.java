/**
* Because this bean correspond to maintain shopping list must be session scope.
*  Otherwise selected recipe will be not access, so list can not be create 
 */
package bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import dataExport.SendDataBeanRemote;
import dataExport.mail.MailRemote;
import recipes.CookingRecipe;
import recipes.Ingredient;
import servlets.Details;
import shoppingList.ShoppingBeanRemote;


/**
 * this class correspond to maintain user shopping list
 * 
 * @author Dawid
 * @since 14.01.2018
 */
public class ShoppingListBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ShoppingBeanRemote shoppingBean;
	@EJB
	private MailRemote mailBean;
	@EJB
	private SendDataBeanRemote mailDocument;
	private CookingRecipe currentRecipe;
	// send to mail address
	private String mail;

	/**
	 * Set current recipe.Must be called before {@link #addToShoppingList()}
	 * @param currentRecipe
	 *            the currentRecipe to set. 
	 */
	public void setCurrentRecipe(CookingRecipe currentRecipe) {
		this.currentRecipe = currentRecipe;
	}

	/**
	 * 
	 * Call {@link ShoppingBeanRemote#addToList(long, String, java.util.List)}
	 */
	public void addToShoppingList() {
		shoppingBean.addToList(currentRecipe.getId(), currentRecipe.getName(), currentRecipe.getIngredients());
	}

	/**
	 * Return ingredients list that was generated as shopping list
	 * 
	 * @return
	 */
	public List<Ingredient> getIngredients() {
		return shoppingBean.getIngredientsList();
	}

	/**
	 * Return map that key is {@link CookingRecipe} name and value is a id
	 * 
	 * @return
	 */
	public Map<String, Long> getNamesAndId() {
		return shoppingBean.getIdAndName();
	}

	/**
	 * This method get parameter open from request and redirect to {@link Details}.
	 */
	public void openHyperlink() {
		//
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			// set display parameter and call redirect to Details servlet
			context.getExternalContext()
					.redirect("Details?display=" + context.getExternalContext().getRequestParameterMap().get("open"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Send mail to provide address
	 */
	public void sendMail() {
		//// get language properties, set mail subject according to user langugae preferences
		ResourceBundle bundle = FacesContext.getCurrentInstance().getApplication()
				.getResourceBundle(FacesContext.getCurrentInstance(), "mainLang");
		//// This get main path string 
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String url = req.getRequestURL().toString();
		String path =url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath();
			//// Create and configure MailBean
		mailBean.configureAddress(mail, new String(bundle.getString("short_title") + " - " + bundle.getString("shopping_list")));
		mailBean.configureList(bundle.getString("shopping_list"), getIngredients());
		mailBean.configureHyperlink(path, "Details","display",bundle.getString("recipes"), getNamesAndId());
		////	call bean send method 	
		mailDocument.send(mailBean);

		mail = null;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail
	 *            the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * 
	 */
	public void clear() {
		shoppingBean.clearList();
	}

}
