/**
 * This class should by rebulid
 */
package bean;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.Part;
import persistenceBean.RecipleBeanRemote;
import recipes.Ingredient;
import types.CategoryType;
import types.UnitType;

/**
 * This class is allow user add new reciple
 * 
 * @author Dawid
 * @version 3
 */
public class FormBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3L;
	private String name;
	private String category;
	private String prepare;
	private String ingredientName;
	private String ingredientUnit;
	private Double ingredientValue;
	private Part image;
	private byte[] binImage;
	private boolean imageValid = false;
	private List<String> categoryList = CategoryType.getCategoryTypes();
	private List<Ingredient> ingredients = new LinkedList<Ingredient>();
	private List<String> unitList = UnitType.getUnitTypes();
	@EJB
	private RecipleBeanRemote recipleBean;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the prepare
	 */
	public String getPrepare() {
		return prepare;
	}

	/**
	 * @param prepare
	 *            the prepare to set
	 */
	public void setPrepare(String prepare) {
		this.prepare = prepare;
	}

	@Override
	public String toString() {
		return "FormBean [name=" + name + ", category=" + category + ", prepare=" + prepare + "]" + "{"
				+ ingredients.size() + "}";
	}

	/**
	 * Save recipe
	 */
	public void save() {

		// get properties as resources
		ResourceBundle resources = FacesContext.getCurrentInstance().getApplication()
				.getResourceBundle(FacesContext.getCurrentInstance(), "formLang");
		// call save method from bean, check status, and display appropriate message
		if (this.prepare == null || this.prepare.isEmpty()) {
			System.err.println("[Prepare is empty]");
			FacesContext.getCurrentInstance().addMessage("new-form-prepare:form_text_area", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Save fail", resources.getString("prepare_required")));
			return;
		}
		if (this.ingredients.isEmpty()) {
			System.err.println("[Ingredients is empty]");
			FacesContext.getCurrentInstance().addMessage("new-form1:submit", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Save fail", resources.getString("ingredient_list_required")));
			return;
		}
		if (recipleBean.saveReciple(name, category, binImage, prepare, ingredients)) {
			// for success
			FacesContext.getCurrentInstance().addMessage("new-form1:submit",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Save data", resources.getString("submit_succes")));
		} else {

			// for fail
			FacesContext.getCurrentInstance().addMessage("new-form1:submit",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Save fail", resources.getString("submit_fail")));
		}
		this.name = null;
		this.category = null;
		this.prepare = null;
		this.ingredients = new LinkedList<Ingredient>();

		/*
		 * FacesContext.getCurrentInstance().addMessage("new-form3:form_text_area", new
		 * FacesMessage(FacesMessage.SEVERITY_ERROR,"Save fail",resources.getString(
		 * "prepare_required")));
		 */
	}

	/**
	 * Add single ingredient
	 */
	public void addIngredient() {
		Ingredient newIngredient = new Ingredient();
		newIngredient.setName(ingredientName);
		newIngredient.setUnit(ingredientUnit);
		newIngredient.setValue(ingredientValue);
		this.ingredients.add(newIngredient);
		this.ingredientName = null;
		this.ingredientUnit = null;
		this.ingredientValue = null;

	}

	public List<String> getCategoryList() {
		return categoryList;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public List<String> getUnitList() {
		return unitList;
	}

	/**
	 * @return the ingredientName
	 */
	public String getIngredientName() {
		return ingredientName;
	}

	/**
	 * @param ingredientName
	 *            the ingredientName to set
	 */
	public void setIngredientName(String ingredientName) {
		this.ingredientName = ingredientName;
	}

	/**
	 * @return the ingredientUnit
	 */
	public String getIngredientUnit() {
		return ingredientUnit;
	}

	/**
	 * @param ingredientUnit
	 *            the ingredientUnit to set
	 */
	public void setIngredientUnit(String ingredientUnit) {
		this.ingredientUnit = ingredientUnit;
	}

	/**
	 * @return the ingredientValue
	 */
	public Double getIngredientValue() {
		return ingredientValue;
	}

	/**
	 * @param ingredientValue
	 *            the ingredientValue to set
	 */
	public void setIngredientValue(Double ingredientValue) {
		this.ingredientValue = ingredientValue;
	}

	/// > since version 2
	/**
	 * 
	 * @param change
	 */
	public void prepareChanged(ValueChangeEvent change) {
		prepare = change.getNewValue().toString();
	}

	/**
	 * 
	 * @param change
	 */
	public void nameChanged(ValueChangeEvent change) {
		name = change.getNewValue().toString();
		System.err.println("[name]" + name);
	}

	/**
	 * 
	 * @param change
	 */
	public void categoryChanged(ValueChangeEvent change) {
		category = change.getNewValue().toString();
		System.err.println("[Category]" + category);
	}

	/**
	 * @return the image
	 */
	public Part getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(Part image) {
		if (imageValid) {
			this.image = image;
		}
	}
	/// > since version 3
	/**
	 * check if file is not null and has image/jpeg type context , if not disallow to save it 
	 * @param context
	 * @param component
	 * @param value
	 */
	public void validateImage(FacesContext context, UIComponent component, Object value) {
		ResourceBundle resources = FacesContext.getCurrentInstance().getApplication()
				.getResourceBundle(FacesContext.getCurrentInstance(), "formLang");
		if (value == null)
			FacesContext.getCurrentInstance().addMessage("new-form-image:file", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "image-error", resources.getString("image_required")));
		else {
			Part image = (Part) value;
			if (image.getContentType().equals("image/jpeg")) {
				this.imageValid = true;
				FacesContext.getCurrentInstance().addMessage("new-form-image:file",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "image-error", "OK"));
			} else {
				FacesContext.getCurrentInstance().addMessage("new-form-image:file", new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "image-error", "Image must have jpg or jpeg extession"));
			}
		}
	}

	/**
	 * Convert part to byte array
	 */
	public void convetToBinary() {

		if (imageValid) {
			try {
				InputStream inputStream = image.getInputStream();
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				//// copy input stream to output stream
				binImage = new byte[inputStream.available()];
				int size;
				while ((size = inputStream.read(binImage)) != -1) {
					outputStream.write(binImage, 0, size);
				}
				binImage = outputStream.toByteArray();
				System.err.println("[ConvertTo]" + binImage.length);
				inputStream.close();
				outputStream.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
