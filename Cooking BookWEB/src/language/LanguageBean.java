/**
 * This bean has session scope, because must set selected language for all pages 
 */
package language;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

/**
 * languageBean set and allow change prefer language if exist on predefined list
 * 
 * @author Dawid
 *
 */
public class LanguageBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String language;
	/**
	 * predefined language list
	 */
	private static Map<String, String> languages;
	static {

		languages = new LinkedHashMap<String, String>();
		languages.put("English", "en_GB");
		languages.put("Polish", "pl_PL");
	}

	@PostConstruct
	/**
	 * Set user locale or set default specified in faces-config.xml, if user locale
	 * is not supported
	 */
	private void checkAndSetLanguage() {
		// check if supported
		if (languages.containsValue(FacesContext.getCurrentInstance().getViewRoot().getLocale().toString()))
			// set user locale
			this.language = FacesContext.getCurrentInstance().getViewRoot().getLocale().toString();
		else
			// set locale specified as default
			this.language = FacesContext.getCurrentInstance().getApplication().getDefaultLocale().toString();
		
	}

	/**
	 * Return all supported language
	 * @return languages map
	 */
	public Map<String, String> getLanguages() {
		return languages;
	}
	/**
	 * Return current language
	 * @return language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * Set selected language
	 * @param language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * Detect change language by user
	 * @param e
	 */
	public void onLanguageChanged(ValueChangeEvent e) {
		String newLocaleValue = e.getNewValue().toString();
		if (languages.containsValue(newLocaleValue)) {
			language=newLocaleValue;
			FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(newLocaleValue));
	}}
}