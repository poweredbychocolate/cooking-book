package servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistenceBean.RecipleBeanRemote;

/**
 * Servlet implementation class Details. Allow to open recipe using hyperlink, and correspond to deliver image from database
 * 
 * @author Dawid
 * @version 2
 * @since 12.01.2018
 */
@WebServlet(description = "Servlet that allow open user saved link", urlPatterns = { "/Details" })
public class Details extends HttpServlet {
	private static final long serialVersionUID = 2L;
	private String recipeId;
	private String imageId;
	@EJB
	RecipleBeanRemote rBean;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Details() {
		super();
	}

	/**
	 * @throws IOException 	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	//// change method to synchronized solve problem with display correct image on recipe list
	protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//// repice index
		this.recipeId = request.getParameter("display");
		//// check if recipeId is null or empty string and check if is lowest than max		
		try {
			if ((recipeId != null) && (!recipeId.isEmpty()) && (rBean.isRecipeId(Long.parseLong(recipeId)))) {

				// add attribute to request session
				request.getSession().setAttribute("id", recipeId);
				// redirect to detalis page with passed atribute
				response.sendRedirect("reciple_detail.xhtml");
				return;
			}
		} catch (Exception e) {
			// also if recipeId can't be parse as long, return page not found
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

		///> since version 2
		///] read byte array image and send as response
		this.imageId = request.getParameter("print");
		try {
			if ((imageId != null) && (!imageId.isEmpty()) && (rBean.isRecipeId(Long.parseLong(imageId)))) {
				response.reset();
				byte[] image = rBean.findImageById(Long.parseLong(imageId));
				response.setContentType("image/jpeg");
				response.setContentLength(image.length);
				response.getOutputStream().write(image);
				response.flushBuffer();

				return;
			}
		} catch (Exception e) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		///] read byte array image and send as response
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}

}
