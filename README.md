# README #
Coocking books application written by Dawid
This work on wildfly11(jsf,ejb,jpa,javamail)  and mariaDB(hibernate provider).
Additional using iText 7 Library allow save receipt as pdf file.

Wildfly config:
+java:/CookDB - mariaDB connection
+java:/Mail - GMail mail subsystem

Planned changes:
-completion internationalization
+add image stored in database to recpie
-extend category and ingredient units
-modernization of the appearance
-add client for android